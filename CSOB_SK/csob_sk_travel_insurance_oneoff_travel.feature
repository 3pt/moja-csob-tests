Feature: Test of Travel insurance Form  

Background:
Given I'm public visitor of travel insurance section of csob #https://www.csob.sk/online/cestovne-poistenie

Scenario: Possitive insurance calculation
#Purpose of the scenario is to verify if is the page calculating the price correctly

Given I select <recurence> and <area> of travel from <date start> to <date end>
And I select <to 15> and <16 to 70> and <above 70> number of travelers
And I select <type> of travel and I travel with <vehicle>
Then I should pay <price premium> EUR for my premium insurance, <price optimal> EUR for my optimal insurance
and I should pay <price demanding> EUR for my demanding insurance

Examples:
| area   | recurence   | date start | date end   | to 15 | 16 to 17 | above 70 | type             | vehicle | price premium | price optimal | price demanding |
| Europe | jednorazova | 01.01.2017 | 02.01.2017 | 0     | 1        | 0        | nemanualna praca | car     | 3.4           | 2.44          | 4.7             |
| World  | jednorazova | 01.01.2017 | 05.05.2017 | 1     | 2        | 1        | turisticka       | car     | 36.75         | 28.55         | 48.35           |


Scenario: Negative insurance calculation
#Purpose of the scenario is to verify if is the front end messages are displayed correctly

Given I select <recurence> and <area> of travel from <date start> to <date end>
And I select <to 15> and <16 to 70> and <above 70> number of travelers
And I select <type> of travel and I travel with <vehicle>
Then I should get <error> message displayed under the selection box 

Examples:
| area   | recurence   | date start | date end   	| to 15 | 16 to 17 | above 70 | type             | vehicle | error 														| 
| Europe | jednorazova | 01.01.2017 | 02.01.2017 	| 0     | 0        | 0        | nemanualna praca | car     | Zvoľte aspoň jednu osobu z dostupných vekových kategórií.	| 
| World  | jednorazova | 03.01.2017 | 01.01.2017 	| 0     | 1        | 0        | turisticka       | car     | Vyberte minulý dátum.								        |
| World  | Opakovana   | 01.01.2017 | N/A	 		| 0     | 0        | 0        | turisticka       | car     | Povinný údaj.					     						|
| World  | Opakovana   | 01.01.2015 | N/A	 		| 0     | 1        | 0        | turisticka       | car     | Vyberte minulý dátum.				       					|
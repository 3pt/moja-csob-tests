'use strict';

var expect = require('chai').expect

module.exports = function() {
    this.World = require('../global-support/world.js').World

    var focusedText = null

    /**
     * impl: 5min
     */
    this.Given(/^I'm public visitor of press release section$/, {timeout: 10 * 1000}, function () {
        return this.driver.get('https://www.csob.sk/o-nas/press/tlacove-spravy')
    })

    this.Then(/^the text which is hyperlink has a blue color$/, function (callback) {
        if(!focusedText){
            callback(new Error("First call 'I see detail of the press release from 'arg1' 'arg2''"))
        } else {
            focusedText.getCssValue("color").then(function(css) {
                if (css == "rgba(0, 51, 102, 1)"){
                    callback()
                } else {
                    callback(new Error("There is no title with blue color ...'"))
                }
            })
        }

    })

    /**
     * impl: 30min
     * (problem : how to use CSS selector finder, how to get text / future objects)
     */
    this.Given(/^I see detail of the press release from january (\d+) "([^"]*)"$/, {timeout: 6 * 1000}, function (arg1, arg2, callback) {
        this.driver.findElements({ css: ".news-listing-title"})
            .then(function(elements){
                var proccessElement = function(callback, elements, titleToFind, index){
                    if(index < 0 || index >= elements.length){
                        return new callback(new Error("There is no title with text '" + arg2 + "'"))
                    }

                    elements[index].getText().then(function(text){
                        if(text == titleToFind){
                            focusedText = elements[index]
                            return callback()
                        } else {
                            index = index + 1
                            return proccessElement(callback, elements, titleToFind, index)
                        }
                    })
                }

                proccessElement(callback, elements, arg2, 0)
            })
    })
}
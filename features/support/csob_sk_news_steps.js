'use strict';

var chai = require("chai");
var chaiAsPromised = require("chai-as-promised");

chai.use(chaiAsPromised);

var expect = chai.expect;

/**
 * TODO :
 * - introduce Page Object for NewsBox
 * - try some async JS framework for then() methods
 */
module.exports = function() {
    this.World = require('../global-support/world.js').World

    var moment = require('moment')

    var clickedNewsTitle = ""
    var newtoEnlargeTitle = ""

    /**
     * impl: 5min
     */
    this.Given(/^I'm public visitor of news section \#https:\/\/www\.csob\.sk\/o\-nas\/novinky$/, {timeout: 10 * 1000}, function () {
        clickedNewsTitle = ""
        return this.driver.get('https://www.csob.sk/o-nas/novinky');
    });

    this.Then(/^I see on the first place from the top the newest news$/, function (callback) {
        this.driver.findElements({ css: ".news-listing-display_date"}).then(function(elements){
            var proccessElement = function(callback, elements, index, previousDate){
                if(index < 0 || index >= elements.length){
                    return callback()
                }

                elements[index].getText().then(function(date){
                    var newDate = moment(date, "dd.MM.YYYY")

                    if(previousDate != null && previousDate < newDate){
                        callback(new Error("News are not sorted from newest od oldest"))
                    }

                    index = index + 1
                    return proccessElement(callback, elements, index, newDate)
                })
            }

            proccessElement(callback, elements, 0, null)

        })
    });

    /**
     * impl: 5min
     */
    this.Then(/^I see maximally (\d+) separate news$/, function (arg1, callback) {
        this.driver.findElements({ css: ".news-listing-display_date"}).then(function(elements){
            if(elements.length > arg1){
                callback(new Error("There is '" + elements.length + "' news, but expected is to be maximum of news " + arg1))
            } else {
                return callback()
            }
        })
    });

    /**
     * impl: 10min
     */
    this.Then(/^I don't see news older than (\d+) months$/, function (arg1, callback) {
        var before = new Date();
        var beforeDate = moment(before.setMonth(before.getMonth() - arg1))

        this.driver.findElements({ css: ".news-listing-display_date"}).then(function(elements){
            var proccessElement = function(callback, elements, index){
                if(index < 0 || index >= elements.length){
                    return callback()
                }

                elements[index].getText().then(function(date){
                    var newDate = moment(date, "dd.MM.YYYY")

                    if(newDate.isBefore(beforeDate, "month")){
                        callback(new Error("There is new with date '" + date + "' older than 3 months"))
                    }

                    index = index + 1
                    return proccessElement(callback, elements, index)
                })
            }

            proccessElement(callback, elements, 0)
        })
    });

    /**
     * Find first from all news, save title to property 'clickedNewsTitle' and click on it -> to open news box
     *
     * impl : 30 mins, problem with need for timeout
     */
    this.When(/^I click on news box$/, function (callback) {
        this.driver.findElements({ css: ".speed-common-dropdown"}).then(function(elements){
            if(elements.length > 0){
                elements[0].findElement({ css: ".news-listing-title"}).then(function(titleElement) {
                    titleElement.getText().then(function(title){
                        clickedNewsTitle = title
                        elements[0].click().then(function () {
                            // TODO : check something like capybara to remove this timeout
                            setTimeout(function(){
                                return callback()
                            }, 1000);

                        })
                    })
                })
            } else {
                callback(new Error("There is 0 news to continue with test"))
            }
        })
    });

    /**
     * Compare if news box title is same as title saved in property 'clickedNewsTitle'
     * Property 'clickedNewsTitle' is set in step 'I click on news box'
     *
     * impl: 30 min, how to share data with other steps
     */
    this.Then(/^I should be able to see News details$/, function (callback) {
        this.driver.findElement({id: "news_detail"}).then(function (newsBox) {
            newsBox.findElement({css: ".news-detail-title"}).then(function (newsBoxTitleElement) {
                expect(newsBoxTitleElement.getText())
                    .to.eventually.be.equal(clickedNewsTitle)
                    .notify(callback)
            })
        })
    });

    /**
     * Always try to click / open first nrews
     * impl: 5 min
     */
    this.Given(/^I see detail of the news$/, function (callback) {
        this.driver.findElements({ css: ".speed-common-dropdown"}).then(function(elements){
            if(elements.length > 0){
                elements[0].click().then(function () {
                    // TODO : check something like capybara to remove this timeout
                    setTimeout(function(){
                        return callback()
                    }, 1000);

                })
            } else {
                callback(new Error("There is 0 news to continue with test"))
            }
        })
    });

    this.Given(/^The news has in its details call to action button$/, function (callback) {
        // Write code here that turns the phrase above into concrete actions
        callback(null, 'pending');
    });

    this.When(/^I click call to action button$/, function (callback) {
        // Write code here that turns the phrase above into concrete actions
        callback(null, 'pending');
    });

    this.Then(/^I should be redirected to target link$/, function (callback) {
        // Write code here that turns the phrase above into concrete actions
        callback(null, 'pending');
    });

    /**
     * Always click on one from the news, which is not enlarged
     * Title is saved into property 'newtoEnlargeTitle'
     *
     * impl: 15min
     */
    this.When(/^i click on collapsed news$/, {timeout: 5 * 1000}, function (callback) {
        this.driver.findElements({ css: ".speed-common-dropdown:not(.speed-common-dropdown_highlight)"}).then(function(elements){
            if(elements.length > 0){
                var newToEnlarge = elements[0]
                newToEnlarge.findElement({ css: ".news-listing-title"}).then(function(titleElement) {
                      titleElement.getText().then(function(title) {
                          newtoEnlargeTitle = title
                          newToEnlarge.click().then(function () {
                              // TODO : check something like capybara to remove this timeout
                              setTimeout(function () {
                                  return callback()
                              }, 1000);
                          })
                      })
                })
            } else {
                callback(new Error("There is 0 collapsed news to continue with test"))
            }
        })
    });

    /**
     * Check if title in news box has different title than first enlarged news item
     * (changes of title in news box is consequence of click on another news item)
     *
     * impl: 10min
     */
    this.Then(/^currently enlarged news will collapse$/, function (callback) {
        this.driver.findElement({id: "news_detail"}).then(function (newsBox) {
            newsBox.findElement({css: ".news-detail-title"}).then(function (newsBoxTitleElement) {
                expect(newsBoxTitleElement.getText())
                    .to.eventually.be.not.equal(clickedNewsTitle)
                    .notify(callback)
            })
        })
    });

    /**
     * In news box - title should be equal to property 'newtoEnlargeTitle'.
     * Property 'newtoEnlargeTitle' is filled in step 'i click on collapsed news'
     *
     * impl: 10min
     */
    this.Then(/^news on which i click will enlarge$/, function (callback) {
        this.driver.findElement({id: "news_detail"}).then(function (newsBox) {
            newsBox.findElement({css: ".news-detail-title"}).then(function (newsBoxTitleElement) {
                expect(newsBoxTitleElement.getText())
                    .to.eventually.be.equal(newtoEnlargeTitle)
                    .notify(callback)
            })
        })
    });

};
'use strict';

module.exports = function() {
    this.World = require('../global-support/world.js').World;

    this.Then(/^I should pay <price demanding> EUR for my demanding insurance$/, function (table, callback) {
        // Write code here that turns the phrase above into concrete actions
        callback(null, 'pending');
    });

    this.Then(/^I should pay <price premium> EUR for my premium insurance, <price optimal> EUR for my optimal insurance$/, function (callback) {
        // Write code here that turns the phrase above into concrete actions
        callback(null, 'pending');
    });

    this.Given(/^I select <type> of travel and I travel with <vehicle>$/, function (callback) {
        // Write code here that turns the phrase above into concrete actions
        callback(null, 'pending');
    });

    this.Given(/^I select <to (\d+)> and <(\d+) to (\d+)> and <above (\d+)> number of travelers$/, function (arg1, arg2, arg3, arg4, callback) {
        // Write code here that turns the phrase above into concrete actions
        callback(null, 'pending');
    });

    this.Given(/^I select <recurence> and <area> of travel from <date start> to <date end>$/, function (callback) {
        // Write code here that turns the phrase above into concrete actions
        callback(null, 'pending');
    });

    /**
     * impl: 5min
     */
    this.Given(/^I'm public visitor of travel insurance section of csob \#https:\/\/www\.csob\.sk\/online\/cestovne\-poistenie$/, function () {
        return this.driver.get('https://www.csob.sk/online/cestovne-poistenie');
    });
};
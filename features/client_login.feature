Feature: Client login to Moja CSOB

Background:
Given I'm unauthorized user
And I have an online CSOB identity
And I'm on login page

Scenario: Succesfull 1st factor login
Given I enter my IPPID and PIN
When I click login
Then I should be redirected to 2nd factor login page

#negative scenarious

Scenario: Wrong IPPID on 1st factor login
Given I enter wrong IPPID and correct PIN
When I click login
Then I should see error msg "Wrong IPPID or PIN"

Scenario: Wrong PIN on 1st factor login
Given I enter my IPPID and wrong PIN
When I click login
Then I should see error msg "Wrong IPPID or PIN"

Scenario: 3 wrong PIN attempts
Given I entered twice wrong PIN
Given I enter my IPPID and wrong PIN
When I click login
Then I should see error msg "Your IPPID was blocked. Visit branch."

#validations on fields

Scenario: IPPID too short
Given I enter less then 8 digits of my IPPID
When I leave focus on IPPID input field
Then I should see error msg "Your IPPID is too short."

Scenario: Non-numberic characters in IPPID
When I enter non-numeric character as IPPID
Then the character is ignored

Scenario: PIN too short
Given I enter less then 5 digits of my PIN
When I leave focus on PIN input field
Then I should see error msg "Your PIN is too short."

Scenario: Non-numberic characters in PIN
When I enter non-numeric character as PIN
Then the character is ignored

#2nd factor login

Scenario: Succesfull 2nd factor login with SMS
Given I succesfuly entered 1st factor
And my authentification device is SMS
When I enter code I recieved
Then I should see my dashboard

Scenario: Succesfull 2nd factor login with OTP
Given I succesfuly entered 1st factor
And my authentification device is token
When I enter OTP
Then I should see my dashboard

#negative scenarious for 2nd factor login

Scenario: Unsuccesfull 2nd factor login with OTP
Given I succesfuly entered 1st factor
When I enter wrong OTP
Then I should see error msg "wrong code"

Scenario: Unsuccesfull 2nd factor login with SMS
Given I succesfuly entered 1st factor
When I enter wrong SMS
Then I should see error msg "wrong code"

Scenario: 5 wrong attempts for 2nd factor login with SMS
Given I succesfuly entered 1st factor
And I enter 4 times wrong SMS
When I enter wrong SMS
Then I should be redirected to 1st factor login
And I should see error msg "wrong code"

Scenario: 5 wrong attempts for 2nd factor login with OTP
Given I succesfuly entered 1st factor
And I enter 4 times wrong OTP
When I enter wrong OTP
Then I should be redirected to 1st factor login
And I should see error msg "wrong code"

#field validations

Scenario: SMS code too short
Given I succesfuly entered 1st factor
And I enter less then 9 digits of SMS code
When I leave focus on SMS code input field
Then I should see error msg "Your code is too short."

Scenario: OTP code too short
Given I succesfuly entered 1st factor
And I enter less then 8 digits of OTP code
When I leave focus on OTP code input field
Then I should see error msg "Your code is too short."

Scenario: Non-numberic characters in SMS code
When I enter non-numeric character as SMS code
Then the character is ignored

Scenario: Non-numberic characters in OTP code
When I enter non-numeric character as OTP code
Then the character is ignored

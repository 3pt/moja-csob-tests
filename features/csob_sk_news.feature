Feature: Client test of public news

Background:
Given I'm public visitor of news section #https://www.csob.sk/o-nas/novinky

Scenario: Newest news
#Puprose is to verify if the newest news is always on top
Then I see on the first place from the top the newest news

Scenario: Ammount of visible news
#Purpose is to make sure that user dont see too many news
Then I see maximally 9 separate news

Scenario: Lattest news
#purpose is that user dont see obsolete news
Then I don't see news older than 3 months

Scenario: News details
#purpose is to verify that user can enlarge details of the news
When I click on news box
Then I should be able to see News details

Scenario: News call to action button
#purpose is to verify that users can use call to action button
Given I see detail of the news
Given The news has in its details call to action button
When I click call to action button
Then I should be redirected to target link

Scenario: only one news can be enlarged
#purpose is to verfy that there can be only one enlarged news at the one time
Given I see detail of the news
When i click on collapsed news
Then currently enlarged news will collapse
And news on which i click will enlarge



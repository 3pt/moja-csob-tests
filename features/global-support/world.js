'use strict';

var webdriver = require('selenium-webdriver');
var platform = process.env.PLATFORM || "CHROME";

var buildChromeDriver = function() {
    return new webdriver.Builder().
    withCapabilities(webdriver.Capabilities.chrome()).
    build();
};

var driver = buildChromeDriver();

var getDriver = function() {
    return driver;
};

var handlers = function () {
    this.registerHandler('AfterFeatures', function (features, callback) {
        driver.quit().then(function(){
            callback()
        })
    });
}

var World = function World() {

    var defaultTimeout = 20000;

    this.webdriver = webdriver;
    this.driver = driver;

};

module.exports = handlers
module.exports.World = World
module.exports.getDriver = getDriver
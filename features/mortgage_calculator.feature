Feature: Public mortgage calculator

Background:
Given I'm visitor of public mortgage calculator

#######
#positive scenarious
#######

#I would use unit tests...would be nice to compare run times of cucu and junit
Scenario: outline: Calculation of interest rate and monthly payment
Given I fill <loan amount>, <value of real estate> and <loan duration>
When I select <fixation duration>
Then I should see <interest rate> and <monthly payment>

#Examples: Common cases
#| <loan amount> | <value of real estate> | <loan duration> | <fixation duration> | <interest rate> | <monthly payment> |
#| 80000		| 80000		 	 | 20		   | 3			 | 2		   | 405	       |
#| 130000	| 150000	 	 | 25		   | 3			 | 1,6		   | 526	       |
#| 30000		| 60000		 	 | 10		   | 3			 | 1,5		   | 269	       |
#
#Examples: Input edge values
#| <loan amount> | <value of real estate> | <loan duration> | <fixation duration> | <interest rate> | <monthly payment> |
#| 5000		| 10000			 | 4		   | 1			 | 3,28		   | 111	       |
#| 5000		| 10000			 | 4		   | 3			 | 1,5		   | 107	       |
#| 5000		| 10000			 | 4		   | 5			 | 1,75		   | 108	       |
#| 300000	| 500000		 | 30		   | 1			 | 3,28		   | 1311	       |
#| 300000	| 500000		 | 30		   | 3			 | 1,5		   | 1035	       |
#| 300000	| 500000		 | 30		   | 5			 | 1,75		   | 1072	       |
#
#Examples: Interest rate edge values
#| <loan amount> | <value of real estate> | <loan duration> | <fixation duration> | <interest rate> | <monthly payment> |
#| 90500		| 100000		 | 30		   | 1			 | 3,48		   | 405	       |
#| 90499		| 100000		 | 30		   | 1			 | 3,38		   | 400	       |
#| 70500		| 100000		 | 30		   | 1			 | 3,38		   | 312	       |
#| 70499		| 100000		 | 30		   | 1			 | 3,28		   | 308	       |
#| 90500		| 100000		 | 30		   | 3			 | 2		   | 335	       |
#| 90499		| 100000		 | 30		   | 3			 | 1,6		   | 317	       |
#| 70500		| 100000		 | 30		   | 3			 | 1,6		   | 247	       |
#| 70499		| 100000		 | 30		   | 3			 | 1,5		   | 243	       |
#| 90500		| 100000		 | 30		   | 5			 | 2,25		   | 346	       |
#| 90499		| 100000		 | 30		   | 5			 | 1,85		   | 328	       |
#| 70500		| 100000		 | 30		   | 5			 | 1,85		   | 255	       |
#| 70499		| 100000		 | 30		   | 5			 | 1,75		   | 252	       |

#Scenario outline: Calculation of interest rate and monthly payment of mortgage for young 

#Scenario outline: Calculation of interest rate for extra repayments

#Scenario: Calculation of interest rate discount for using current account #move to first scenario outline

#Scenario: Online application

#Scenarion: Meeting at branch

#Scenarion: Statistics of unfinished forms

#######
#negative scenarious
#######

#none

#######
#validations of fields
#######

#Scenario outline: out range values for input fields
